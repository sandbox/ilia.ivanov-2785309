<?php
namespace Drupal\eid_auth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserInterface;

/**
 * Class EidAuthController handles the user login and registration.
 */
class EidAuthController extends ControllerBase {

  /**
   * Gets the user data from the server and either directs to register or login.
   */
  public function content() {
    // Cleanup mobile id data if it was iserted before.
    if (isset($_SESSION['mobiil_id'])) {
      unset($_SESSION['mobiil_id']);
    }

    unset($_SESSION['SSL_CLIENT_VERIFY']);

    /**@var \Drupal\eid_auth\EIDAuthServiceInterface $eid */
    $eid = \Drupal::service('eid_auth.connector');

    switch ($eid->state()) {
      case $eid::NONE:
        // No id card.
        drupal_set_message(t('Palun kontrolli, kas ID-kaart on korralikult kaardilugejas!'), 'warning');
        unset($_SESSION['EID_AUTH_DATA']);
        return $this->redirect('user.login');
        break;

      case $eid::SUCCESS:
        // User signed the certificate.
        drupal_set_message(t('Sisselogimine õnnestus!'), 'status');

        /**@var \Drupal\eid_auth\EidAuthUserMatch $user_match */
        $user_match = \Drupal::service('eid_auth.usermatch');
        // Check if user exxists.
        $user = $user_match->userExists($eid->__get("UserIDCode"));
        if ($user instanceof UserInterface && $user->isActive()) {
          /*
           * Check if user data still unchanged and sync it with EID response
           * if different.
           */
          $user_match->checkUserPersonalDataUnchanged($user, $eid);
          // We have user so we ...
          user_login_finalize($user);

          unset($_SESSION['EID_AUTH_DATA']);
          return $this->redirect('<front>');
        }
        else {
          // We have no Drupal user so we will register him.
          return $this->redirect('user.register');
        }

        break;

      default:
        // If something happened that is totally different, throw a message.
        drupal_set_message(t('Tekkis tehniline tõrge, palun proovige uuesti!'), 'error');
        return $this->redirect('user.login');

    }
  }

}
