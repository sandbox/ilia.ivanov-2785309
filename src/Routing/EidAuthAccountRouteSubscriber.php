<?php
/**
 * @file
 * Class Drupal\eid_auth\Routing\EidAuthAccountRouteSubscriber
 */

namespace Drupal\eid_auth\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class EidAuthAccountRouteSubscriber extends RouteSubscriberBase {

  /**
   * Alters existing routes for a specific collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection for adding routes.
   */
  protected function alterRoutes(RouteCollection $collection) {
    if($route =$collection->get('user.login')){
      $route->setDefaults([
        '_title'=> 'Log in with id',
       '_form' => '\Drupal\eid_auth\Form\EidAuthUserLoginForm',
      ]);


    }

  }
}