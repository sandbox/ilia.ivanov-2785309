<?php

/**
 * @file
 * Contains \Drupal\eid_auth\EIDAuthServiceInterface.
 */

namespace Drupal\eid_auth;

/**
 * Interface EIDAuthServiceInterface.
 *
 * @package Drupal\eid_auth
 */
interface EIDAuthServiceInterface {

  const NONE = 1;
  const SUCCESS = 2;

  /**
   * Report on the Connection status
   *
   * @return integer
   *    return one of EIDAuthServiceInterface::NONE or EIDAuthServiceInterface::SUCCESS
   */
  public function state();

  /**
   * Retrieve a list of valid property keys for data retrieved from eID
   *
   * @return string[]
   *   return an array of valid keys for the value method
   */
  public function keys();

  /**
   * Retrieve a single data value from the eID data retreived
   *
   * @param $key
   * @return mixed
   */
  public function __get($key);

  /**
   * Format login data.
   *
   * Names with "-" should be properly cased.
   *
   * @param string $value
   *   Login data input: name and surname.
   * @param string $separator
   *   Separator.
   *
   * @return null|string
   *   Name and surname.
   */
  public function formatLoginData($value, $separator = '-');

}
