<?php

/**
 * @file
 * Contains \Drupal\eid_auth\Form\EidAuthMobIdChallenge.
 */

namespace Drupal\eid_auth\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\user\UserInterface;

/**
 * Class EidAuthMobIdChallenge.
 *
 * @package Drupal\eid_auth\Form
 */
class EidAuthMobIdChallenge extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eid_auth_mob_id_challenge';
  }

  /**
   * {@inheritdoc}
   *
   * Will create form that will display challenge nubmer + create ajax request
   * to mobile id service. When user will accept or not page will recieve ajax
   * callback and then function will save response to $_SESSION, also form will
   * be submutted by javascript.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $challenge_id = $_SESSION['mobiil_id']['challengeid'];

    $markup = '<span class="mobid-challenge">' . $this->t('Mobiil-ID kontrollkood on <span id=":mobid-challenge-num">:challengeId</span>', array(':challengeId' => $challenge_id)) . '</span>';

    $form['description'] = array(
      '#type' => 'markup',
      '#markup' => $markup,
      '#tree' => TRUE,
    );

    $form['auth_status'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Auth Status'),
      '#maxlength' => 64,
      '#size' => 64,
      '#ajax' => [
        'callback' => array($this, 'validateMobId'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Verifying...'),
        ),
      ],
      '#suffix' => '<span class="srv-message"></span>',
      '#attributes' => array(
        'class' => array('hidden'),
      ),
    );

    $form['mobid_submit_checkbtn'] = array(
      '#type' => 'submit',
      '#name' => 'add',
      '#value' => t('Submit'),
      '#submit' => array(array($this, 'submitForm')),
      '#attributes' => array(
        'class' => array('hidden'),
      ),
    );

    $form['#attached']['library'][] = 'eid_auth/eid_auth_mobid_forms';

    $form["#cache"]['max-age'] = 0;

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * After form was submitted check for a response and direct user to login
   * service or back to login page where he will choose auth method. Also this
   * function will display error message if auth was not successful.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (isset($_SESSION['mobiil_id']['soap_result_status'])) {
      switch ($_SESSION['mobiil_id']['soap_result_status']) {
        case "USER_AUTHENTICATED":
          /**@var \Drupal\eid_auth\EIDAuthServiceInterface $eid */
          $eid = \Drupal::service('eid_auth.connector');

          drupal_set_message(t('Sisselogimine õnnestus!'), 'status');

          /**@var \Drupal\eid_auth\EidAuthUserMatch $user_match */
          $user_match = \Drupal::service('eid_auth.usermatch');
          $user = $user_match->userExists($eid->__get("UserIDCode"));
          if ($user instanceof UserInterface && $user->isActive()) {
            /*
             * Check if user data still unchanged and sync it with EID
             * response if different.
             */
            $user_match->checkUserPersonalDataUnchanged($user, $eid);
            // We have user so we ...
            user_login_finalize($user);

            $form_state->setRedirect('<front>');
          }
          else {
            $form_state->setRedirect('user.register');
          }
          break;

        default:
          drupal_set_message(_eid_auth_get_error_message($_SESSION['mobiil_id']['soap_result_status']), 'warning');
          $form_state->setRedirect('eid_auth.mobid_auth_form');
      }

    }
    else {
      drupal_set_message(_eid_auth_get_error_message(100), 'warning');
      $form_state->setRedirect('eid_auth.mobid_auth_form');
    }
  }


  /**
   * {@inheritdoc}
   *
   * Will "validate" field. After validation will be triggered function will
   * call soap service and make request to mobile id service.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   response.
   */
  public function validateMobId(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $param = array(
      'Sesscode' => $_SESSION['ddsSessionCode'],
      'WaitSignature' => TRUE,
    );

    $soap_result = $this->mobileIdStepOne($param);
    $_SESSION['mobiil_id']['soap_result_status'] = $soap_result['Status'];
    $message = $soap_result['Status'];
    $form_state->setRedirect('eid_auth.mobid_auth_form');

    $response->addCommand(new HtmlCommand('.srv-message', $message));
    return $response;

  }


  /**
   * {@inheritdoc}
   *
   * Soap request to get GetMobileAuthenticateStatus.
   *
   * @param array $param
   *   An array of the arguments to pass to the function. This can be either
   *   an ordered or an associative array. Note that most SOAP servers require
   *   parameter names to be provided, in which case this must be an
   *   associative array.
   *
   * @return mixed
   *   result.
   */
  private function mobileIdStepOne($param) {
    $config = $this->config('ekk_eid_auth.settings');
    $client = new \SoapClient(
      $config->get('eid_auth_server_url'),
      array(
        'trace' => TRUE,
        'exceptions' => FALSE,
      )
    );
    $result = $client->__soapCall("GetMobileAuthenticateStatus", $param);

    return $result;
  }

}
