<?php

/**
 * @file
 * Contains \Drupal\ekk_eid_auth\Form\ModuleConfigurationForm.
 */

namespace Drupal\eid_auth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Defines a form that configures forms module settings.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ekk_eid_auth_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ekk_eid_auth.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    // Load config.
    $config = $this->config('ekk_eid_auth.settings');
    // Get user entity.
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    // Load its fields.
    $user_fields = $user->getFieldDefinitions();
    // Fill array with those fields where field machine name will be array key
    // and field name will be value.
    $select_options = array();
    foreach ($user_fields as $key => $field) {
      $select_options[$key] = $field->getLabel();
    }
    // Label.
    $form['eid_auth_profile_mappings_label'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Mapped user profile fields'),
      '#description' => $this->t('Here you should specify user profile fields to make everything work.'),
    );
    // Link everything that fields will go under the fieldset.
    $profile_mappings = &$form['eid_auth_profile_mappings_label'];
    $profile_mappings['eid_auth_mapped_first_name'] = array(
      '#type' => 'select',
      '#title' => $this->t('First name'),
      '#options' => $select_options,
      '#default_value' => $config->get('eid_auth_mapped_first_name'),
      '#description' => "Select the field what defines users first name.",
    );
    $profile_mappings['eid_auth_mapped_last_name'] = array(
      '#type' => 'select',
      '#title' => $this->t('Last name'),
      '#options' => $select_options,
      '#default_value' => $config->get('eid_auth_mapped_last_name'),
      '#description' => "Select the field what defines users last name.",
    );
    $profile_mappings['eid_auth_mapped_phone'] = array(
      '#type' => 'select',
      '#title' => $this->t('Mobile number'),
      '#options' => $select_options,
      '#default_value' => $config->get('eid_auth_mapped_phone'),
      '#description' => "Select the field what defines users mobile id number.",
    );
    $profile_mappings['eid_auth_mapped_id'] = array(
      '#type' => 'select',
      '#title' => $this->t('ID number'),
      '#options' => $select_options,
      '#default_value' => $config->get('eid_auth_mapped_id'),
      '#description' => "Select the field what defines users id number.",
    );

    $form['eid_auth_server_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('URL for DigiDoc service server'),
      '#default_value' => $config->get('eid_auth_server_url', 'https://tsp.demo.sk.ee/?wsdl '),
      '#description' => "For testing use: https://tsp.demo.sk.ee/?wsdl",
    );

    $form['eid_auth_server_username'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Username for server'),
      '#default_value' => $config->get('eid_auth_server_username'),
    );

    $form['eid_auth_server_password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Password for server'),
      '#default_value' => $config->get('eid_auth_server_password'),
    );

    $form['eid_auth_service_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Service name'),
      '#description' => $this->t('Name of your Mobiil-ID service. With the OpenXAdES test server you can use the service name "Testimine", but do not use it for a production site! To register your own service visit the web site of !name at !url. For testing use `Testimine`.'),
      '#default_value' => $config->get('eid_auth_service_name'),
    );

    $form['eid_auth_ddoc_file_location'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Location for DigiDoc files to be saved at. Provided path must use Drupal public or private path wrapper.'),
      '#default_value' => $config->get('eid_auth_ddoc_file_location'),
      '#description' => $this->t('The directory must be writeable. For example `public://ddoc/`'),
    );

    $form['eid_auth_signer_defaults'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Default values for signer information for digital signing. Can be left empty if signing is not used'),
      '#description' => $this->t('These are just default values, actual values can be set before every signing by implementing _eid_auth_signer_alter hook.'),
    );
    $signer_default = &$form['eid_auth_signer_defaults'];
    $signer_default['eid_auth_signer_role'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Role'),
      '#default_value' => $config->get('eid_auth_signer_role'),
    );
    $signer_default['eid_auth_signer_city'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#default_value' => $config->get('eid_auth_signer_city'),
    );
    $signer_default['eid_auth_signer_state'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('State or province'),
      '#default_value' => $config->get('eid_auth_signer_state'),
    );
    $signer_default['eid_auth_signer_postalcode'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Postal code'),
      '#default_value' => $config->get('eid_auth_signer_postalcode'),
    );
    $signer_default['eid_auth_signer_country'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Country code'),
      '#default_value' => $config->get('eid_auth_signer_country'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Will run over the settings and save those to database.
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      switch ($key) {
        case "eid_auth_server_url":
        case "eid_auth_server_username":
        case "eid_auth_server_password":
        case "eid_auth_service_name":
        case "eid_auth_ddoc_file_location":
        case "eid_auth_signer_role":
        case "eid_auth_signer_city":
        case "eid_auth_signer_state":
        case "eid_auth_signer_postalcode":
        case "eid_auth_signer_country":
        case "eid_auth_mapped_first_name":
        case "eid_auth_mapped_last_name":
        case "eid_auth_mapped_phone":
        case "eid_auth_mapped_id":
          $this->config('ekk_eid_auth.settings')
            ->set($key, $value)
            ->save();
          break;

        default:
          continue;
      }
    }
  }
}
