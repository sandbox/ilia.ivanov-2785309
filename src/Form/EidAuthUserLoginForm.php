<?php
/**
 * @file
 * Contains: \Drupal\eid_auth\Form\EidAuthUserLoginForm
 */

namespace Drupal\eid_auth\Form;


use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Form\UserLoginForm;

class EidAuthUserLoginForm extends UserLoginForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    global $language;

    // Cleanup mobile id data if it was iserted before.
    if (isset($_SESSION['mobiil_id'])) {
      unset($_SESSION['mobiil_id']);
    }

    if (isset($_SESSION['EID_AUTH_DATA'])) {
      unset($_SESSION['EID_AUTH_DATA']);
    }

    $form['ID-label']= [
      '#prefix' => '<div class="visible-fields-wrapper"><div class="label-and-field-wrapper">',
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => t('Login with ID Card', array(), array('context' => 'eid_auth')),
    ];
    $form['id_card_submit'] = [
      '#prefix' => '<span class="button-wrapper">',
      '#type' => 'submit',
      '#value' => t('Login with ID Card', array(), array('context' => 'eid_auth')),
      '#title' => t('Login with ID Card', array(), array('context' => 'eid_auth')),
      '#name' => 'id_card_submit',
      '#suffix' => '</span></div>',
    ];
    $form['MOBID-label']= [
      '#prefix' => '<div class="label-and-field-wrapper">',
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => t('Login with Mobile ID', array(), array('context' => 'eid_auth')),
    ];
    $form['mobile_id_submit'] = [
      '#prefix' => '<span class="button-wrapper">',
      '#type' => 'submit',
      '#value' => t('Login with Mobile ID', array(), array('context' => 'eid_auth')),
      '#title' => t('Login with Mobile ID', array(), array('context' => 'eid_auth')),
      '#name' => 'mobile_id_submit',
      '#suffix' => '</span></div></div>',
    ];
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $form['eid_auth_url'] = array(
      '#type' => 'textfield',
      '#maxlength' => 64,
      '#size' => 64,
      '#attributes' => array(
        'class' => array('hidden'),
      ),
      '#default_value' => $base_url . "/eid-auth",
    );
    $form['eid_auth_error_msg'] = array(
      '#type' => 'textfield',
      '#maxlength' => 256,
      '#size' => 256,
      '#attributes' => array(
        'class' => array('hidden'),
      ),
      '#default_value' => t('Kasutaja tuvastamine ebaõnnestus! Palun kontrollige, kas Teie ID-kaart on kehtiv ja kaardilugejasse korrektselt sisestatud.'),
    );
    $form['#attached']['library'][] = 'eid_auth/eid_auth_eid_auth_check';
    $form['#attached']['library'][] = 'eid_auth/eid_form_styling';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (array_key_exists('id_card_submit', $form_state->getUserInput())) {
      $form_state->setRedirect('eid_auth.content');
    }
    elseif (array_key_exists('mobile_id_submit', $form_state->getUserInput())) {
      $form_state->setRedirect('eid_auth.mobid_auth_form');
    }
    else {
      drupal_set_message($this->t('Undefined submit type!'), 'error');
      $form_state->setRedirect('user.login');
    }
  }
}
