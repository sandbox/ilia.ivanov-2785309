<?php
namespace Drupal\eid_auth\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'EID auth block' block.
 *
 * @Block(
 *  id = "eid_auth_block",
 *  admin_label = @Translation("Estonian ID cart auth block."),
 * )
 */

class EidAuthBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array('#markup' =>
      '<span class="login-with-idcard">' . $this->t('<a href=":id_login_page">Login with ID card</a>', array(':id_login_page' => '/eid-auth')) . '</span>'
      .'<span class="login-with-mobileid">' . $this->t('<a href=":mobidchallenge">Login with Mobile ID</a>', array(':mobidchallenge' => '/mobid-auth')) . '</span>'
    );
  }
}