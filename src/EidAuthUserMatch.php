<?php

/**
 * @file
 * Contains \Drupal\eid_auth\EidAuthUserMatch.
 */

namespace Drupal\eid_auth;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\user\UserInterface;

/**
 * Class EidAuthUserMatch.
 */
class EidAuthUserMatch {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory $queryFactory
   */
  protected $queryFactory;

  /**
   * EidAuthUserMatch constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity Type Manager.
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   Query Factory.
   */
  public function __construct(EntityTypeManager $entity_type_manager, QueryFactory $query_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queryFactory = $query_factory;
  }

  /**
   * Check if user already exists.
   *
   * Check if user with passed personal code already exists.
   *
   * @param string $identification_number
   *   eID String identifier form the auth service.
   *
   * @return \Drupal\user\UserInterface|false
   *   existing user object or false.
   */
  public function userExists($identification_number) {

    $user_storage = $this->entityTypeManager->getStorage('user');
    $user_query = $this->queryFactory->get('user');

    /** @var [] $userResults */
    $user_results = $user_query->condition('field_identification_number.value', $identification_number, '=')
      ->range(0, 1)
      ->execute();

    if (count($user_results) > 0) {
      // User exists, let's login.
      /** @var int $user_id */
      $user_id = reset($user_results);

      /** @var \Drupal\user\UserInterface $user */
      $user = $user_storage->load($user_id);

      if ($user instanceof UserInterface) {
        return $user;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Check user data is unchanged.
   *
   * Check if user data is still the same as in the EstID service responce.
   *
   * @param \Drupal\user\UserInterface $user
   *   User object.
   * @param \Drupal\eid_auth\EIDAuthServiceInterface $eid
   *   EID service object.
   */
  public function checkUserPersonalDataUnchanged(UserInterface $user, EIDAuthServiceInterface $eid) {
    /** @var \Drupal\ekk_user_login\EkkUserLoginService $user_login_helpers */
    $user_login_helpers = \Drupal::service('ekk_user_login.helpers');

    $s_name = $eid->formatLoginData($eid->__get('UserGivenname'));
    $s_surname = $eid->formatLoginData($eid->__get('UserSurname'));
    $s_birth_date = $user_login_helpers->getBirthDateFromIdNumber();

    if (!empty($s_name) && !empty($s_surname) && !empty($s_birth_date)) {
      $data_changed = $s_name != $user->get('field_first_name')->getValue()[0]['value']
        || $s_surname != $user->get('field_last_name')->getValue()[0]['value']
        || $s_birth_date != $user->get('field_birth_date')->getValue()[0]['value'];

      if ($data_changed) {
        $user->set('field_first_name', $s_name);
        $user->set('field_last_name', $s_surname);
        $user->set('field_birth_date', $s_birth_date);
        $user->save();
      }
    }

  }

}
