<?php
/**
 * @file
 * Contains \Drupal\eid_auth\EIDAuthService.
 */

namespace Drupal\eid_auth;


/**
 * Class EIDAuthService.
 *
 * @package Drupal\eid_auth
 */
class EIDAuthService implements EIDAuthServiceInterface {

  static private $state;
  static private $data = [];

  /**
   * Constructor.
   */
  public function __construct() {
    static::$state = EIDAuthServiceInterface::NONE;
    static::$data = array(
      'UserGivenname' => '',
      'UserSurname' => '',
      'UserIDCode' => 00000000000,
    );
    // TODO: remove mock data.
    $_SESSION['SSL_CLIENT_VERIFY'] = 'IVANOV,ILIA,38608290042';

    if (empty($_SESSION['SSL_CLIENT_VERIFY'])) {
      $_SESSION['SSL_CLIENT_VERIFY'] = $_SERVER['SSL_CLIENT_VERIFY'] ? $_SERVER['SSL_CLIENT_VERIFY'] : $_SERVER['REDIRECT_SSL_CLIENT_VERIFY'];
    }

    // Check if user was authorised by mobile id or id card.
    if ($_SESSION['SSL_CLIENT_VERIFY']) {
      static::$state = EIDAuthServiceInterface::SUCCESS;

      // TODO: remove mock data.
      $_SERVER['SSL_CLIENT_S_DN_CN'] = 'IVANOV,ILIA,38608290042';

      if (isset($_SERVER['SSL_CLIENT_S_DN_CN'])
        && !is_null($_SERVER['SSL_CLIENT_S_DN_CN'])
      ) {
        $_SESSION['EID_AUTH_DATA'] = $_SERVER['SSL_CLIENT_S_DN_CN'];
      }

      if (isset($_SERVER['REDIRECT_SSL_CLIENT_S_DN_CN'])
        && !is_null($_SERVER['REDIRECT_SSL_CLIENT_S_DN_CN'])
      ) {
        $_SESSION['EID_AUTH_DATA'] = $_SERVER['REDIRECT_SSL_CLIENT_S_DN_CN'];
      }

      $exploded_data = explode(",", $_SESSION['EID_AUTH_DATA']);

      static::$data = array(
        'UserGivenname' => $this->formatLoginData($exploded_data[1]),
        'UserSurname' => $this->formatLoginData($exploded_data[0]),
        'UserIDCode' => $exploded_data[2],
      );
    }

    if (isset($_SESSION['mobiil_id']['soap_result_status'])
      && $_SESSION['mobiil_id']['soap_result_status'] == "USER_AUTHENTICATED"
    ) {
      static::$state = EIDAuthServiceInterface::SUCCESS;
      static::$data = array(
        'UserGivenname' => $this->formatLoginData($_SESSION['mobiil_id']['firstname']),
        'UserSurname' => $this->formatLoginData($_SESSION['mobiil_id']['lastname']),
        'UserIDCode' => $_SESSION['mobiil_id']['idcode'],
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function state() {
    return static::$state;
  }

  /**
   * {@inheritdoc}
   */
  public function keys() {
    return array_keys(static::$data);
  }

  /**
   * {@inheritdoc}
   */
  public function __get($key) {
    if (isset(static::$data[$key])) {
      return static::$data[$key];
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formatLoginData($value, $defined_separator = NULL) {
    // >>> CALLED RECURSIVELY!
    $result = NULL;
    $separators = [' ', '-'];
    if ($defined_separator) {
      $separator = $defined_separator;
      $exploded_value = explode($separator, $value);
      if (is_array($exploded_value)) {
        $parts = [];
        foreach ($exploded_value as $name_part) {
          $parts[] = self::mbUcfirst(mb_strtolower($name_part));
        }
        $result = implode($separator, $parts);
      }
      else {
        $result = self::mbUcfirst(mb_strtolower($value));
      }
    }
    else {
      $tmp_value = $value;
      $separator_exists = FALSE;
      foreach ($separators as $separator) {
        if (strpos($tmp_value, $separator)) {
          $tmp_value = $this->formatLoginData($tmp_value, $separator);
          $separator_exists = TRUE;
        }
      }
      if (!$separator_exists) {
        $tmp_value = self::mbUcfirst(mb_strtolower($tmp_value));
      }
      $result = $tmp_value;
    }

    return $result;
  }

  /**
   * Upper case strings with umlauts.
   *
   * @param string $string
   *   String to convert.
   * @param string $encoding
   *   Encoding.
   *
   * @return string
   *   upper-cased mb string
   */
  public static function mbUcfirst($string, $encoding = 'UTF-8') {
    $length = mb_strlen($string, $encoding);
    $first_char = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $length - 1, $encoding);

    return mb_strtoupper($first_char, $encoding) . $then;
  }

}
