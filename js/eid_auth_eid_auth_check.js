(function ($) {
    Drupal.behaviors.eid_Behavior = {
        attach: function (context, settings) {
            $(document).ready(function(){
                // Flag that will prevent form from submission.
                var submission_flag = false;
                $("#user-login-form").submit(function (e) {
                    // Get clicked button.
                    var button_clicked = $(this).find("input[type=submit]:focus");
                    // Get its id.
                    var button_id = button_clicked[0].id;
                    if (submission_flag == false && button_id == "edit-id") {
                        // Prevent default submission.
                        e.preventDefault();
                        // Get id card endpoint location.
                        var endpoint_url = $("#edit-eid-auth-url").val();
                        // Get error message string.
                        var endpoint_error_msg = $("#edit-eid-auth-error-msg").val();
                        // Make request to endpoint.
                        $.get(endpoint_url, function (data, status) {
                        }).done(function () {
                            // Unset flag.
                            submission_flag = true;
                            // Click on Login with id card button.
                            $("#edit-id").trigger("click");
                        }).fail(function (error) {
                            // Inform user that something is wrong.
                            alert(endpoint_error_msg);
                        });
                    }
                });
            });
        }
    }
})(jQuery);