(function ($) {
    Drupal.behaviors.eui_Behavior = {
        attach: function (context, settings) {
            $(document).ready(function(){
                $(".srv-message").html(" ");
                $(".srv-message").bind("DOMSubtreeModified",function(){
                    $("#edit-mobid-submit-checkbtn").trigger("click");
                });
                setTimeout(eid_auth_mobid_forms_trigger(), 5000);

                function eid_auth_mobid_forms_trigger(){
                    $("#edit-auth-status").trigger("change");
                }
            });
        }
    }
})(jQuery);